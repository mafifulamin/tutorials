extends KinematicBody2D

export (int) var speed = 400
export (int) var jump_speed = -600
export (int) var GRAVITY = 2000

const UP = Vector2(0,-1)

var velocity = Vector2()
var has_double_jump = false

func get_input():
	var animation = "idle"
	velocity.x = 0
	if Input.is_action_just_pressed('up'):
		if is_on_floor():
			velocity.y = jump_speed
			has_double_jump = false
		elif (not is_on_floor()) and (not has_double_jump):
			velocity.y = jump_speed
			has_double_jump = true
	if Input.is_action_pressed("right"):
		velocity.x += speed
		animation = "jump_kanan"
		if is_on_floor():
			animation = "jalan_kanan"
		if Input.is_action_pressed("ui_select"):
			velocity.x += speed
		if Input.is_action_just_pressed("down"):
			position.x += 100
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		animation = "jump_kiri"
		if is_on_floor():
			animation = "jalan_kiri"
		if Input.is_action_pressed("ui_select"):
			velocity.x -= speed
		if Input.is_action_just_pressed("down"):
			position.x -= 100
			
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
