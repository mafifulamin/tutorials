extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 2000
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()
var jumping = false

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed('jump'):
		jumping = true
		velocity.y = jump_speed * 2
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	print(jumping)
	if velocity.y > 0:
		jumping = false
	if velocity.y != 0 and jumping == true:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		animator.play("Idle")
